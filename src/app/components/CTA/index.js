import React from 'react'
import {StyleSheet, css} from 'aphrodite/no-important'

class CTA extends React.Component {
    constructor(props){
        super(props);
    }

    handleClick() {
        if(typeof this.props.onClick === 'function'){
            this.props.onClick()
        }else{
            window.location = this.props.onClick;
        }
    }

    render() {
        const style = StyleSheet.create({
            cta: {
                padding: '5px 10px',
                borderRadius: 20,
                border: '2px solid ' + (this.props.colour || '#313948'),
                color: this.props.colour || '#313948',
                fontWeight: 'bold',
                textDecoration: 'none',
                display: 'inline-block',
                userSelect: 'none',
                cursor: 'pointer'
            }
        });

        return(
            <a className={css(style.cta)} onClick={()=>this.handleClick()}>{this.props.children}</a>
        )
    }
}

export default CTA;