import React from 'react';
import {StyleSheet, css} from 'aphrodite/no-important';

class Section extends React.Component {
    constructor(props){
        super(props);
    }


    render() {
        let backgroundPosition = 'center';
        const {style} = this.props;
        const styles = StyleSheet.create({
           section: {
               position: 'relative',
               background: !this.props.backgroundFade ? (this.props.backgroundUrl ? `url('${this.props.backgroundUrl}') 0 0 no-repeat` : null) : null,
               backgroundAttachment: !this.props.backgroundFade ? (this.props.paralax ? 'fixed' : 'initial') : null,
               backgroundPositionY: !this.props.backgroundFade ? (this.props.backgroundBottom ? 'bottom' : 'top') : null,
           },
            background: {
               position: 'absolute',
                background: this.props.backgroundUrl ? `url('${this.props.backgroundUrl}') 0 0 no-repeat` : null,
                backgroundAttachment: this.props.paralax ? 'fixed' : 'initial',
                backgroundPositionY: this.props.backgroundBottom ? 'bottom' : 'top',
                top: 0,
                right: 0,
                bottom: 0,
                left: 0,
                opacity: 'windowProperties' in this.props ? 1-(this.props.windowProperties.scroll/this.props.windowProperties.height) : 1
            },
            content: {
                width: '100%',
                maxWidth: 760,
                margin: '0 auto',
                padding: '10px 0'
            }
        });

        return (
            <section className={css(styles.section)} style={style}>
                {this.props.backgroundFade ? <div className={css(styles.background)}/> : null }
                {this.props.pad ? <div className={css(styles.content)}>{this.props.children}</div> : this.props.children}
            </section>
        )
    }
}

export default Section;