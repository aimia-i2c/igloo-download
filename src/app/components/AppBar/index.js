import React from 'react'
import {StyleSheet, css} from 'aphrodite/no-important'


class AppBar extends React.Component {
    constructor(props){
        super(props);
    }

    render() {
        const styles = StyleSheet.create({
            appbar: {
                background: '#303948',
                color: '#FFFFFF',
                padding: 10
            }
        });

        return (
            <div className={css(styles.appbar)}>Igloo</div>
        )
    }
}

export default AppBar