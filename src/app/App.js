import React from 'react'
import Section from './components/Section'
import CTA from './components/CTA';
import {StyleSheet, css} from 'aphrodite/no-important'

import imageScene1 from '../images/scene.png';
import appIcon from '../images/app_icon.png';
import headerimage from '../images/header.png';
import footerimage from '../images/footer.png';

class App extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            windowHeight: window.innerHeight,
            windowWidth: window.innerWidth,
            windowScroll: window.scrollY
        }
    }

    componentDidMount() {
        window.onresize = (event) => {
            this.handleWindowResize();
        };

        window.onscroll = () => {
            this.setState({windowScroll: window.scrollY});
        }

    }

    handleWindowResize() {
        this.setState({windowWidth: window.innerWidth, windowHeight: window.innerHeight})
    }

    handleHelp() {
        window.scrollTo(0, this.state.windowHeight)
    }

    render() {
        const globalStyle = StyleSheet.create({
            global: {
                fontFamily: 'Avenir, Helvetica, sans-serif',
                size: '12pt'
            },
            a: {
                color: '#3585F0'
            },
            h2: {
                marginBottom: 5,
                fontWeight: 'normal'
            }
        });

        return(
            <div className={css(globalStyle.global)}>
                <Section style={{height: this.state.windowHeight, backgroundPosition: 'top'}} paralax={true} backgroundUrl={headerimage} backgroundFade={true} windowProperties={{height: this.state.windowHeight, scroll: this.state.windowScroll}}>
                    <div style={{position: 'absolute', top: '50%', marginTop: -150, textAlign: 'center', left: 0, right: 0}}>
                        <img src={appIcon} style={{height: 200, marginBottom: 20}} /> <br />
                        <CTA onClick="igloo.exe">
                            Download for Windows
                        </CTA>
                        <div style={{marginTop: 20, fontSize: '90%', textDecoration: 'underline', cursor: 'pointer'}} onClick={() => this.handleHelp()}>FAQ</div>
                    </div>
                </Section>
                <Section ref={(sec) => {this.Help = sec}} style={{height: this.state.windowHeight, backgroundPosition: 'bottom'}} backgroundUrl={footerimage} pad={true}>
                    <div style={{marginTop: 20, fontSize: '90%', textDecoration: 'underline', cursor: 'pointer'}} onClick={() => window.scrollTo(0,0)}>Back</div>

                    <h1>FAQ</h1>
                    <h2 className={css(globalStyle.h2)}>Do I need local admin to install?</h2>
                    The app is installed differently to most software packages, eliminating the need for IT support or local admin rights. You should be able to run the installer without issue.

                    <h2 className={css(globalStyle.h2)}>The App will not install</h2>
                    If you are having difficulty installing igloo, please <a className={css(globalStyle.a)} href="mailto:igloo@aimia.com">reach out</a> to us for assistance.

                    <h2 className={css(globalStyle.h2)}>How can I request a new module?</h2>
                    We would love to see igloo grow, and your ideas will help us do that. To discuss your module idea, please contact <a className={css(globalStyle.a)} href="mailto:john.durham@i-2-c.co.uk">John Durham</a>.
                </Section>
            </div>
        )
    }
}

export default App;