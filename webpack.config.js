const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');

module.exports = {
    entry: {
        app: ['./src/app']
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loaders: ['babel-loader'],
                exclude: /node_modules/
            },
            {
                test   : /\.(ttf|eot|svg|woff|jpg|png)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader : 'file-loader'
            }
        ]
    },
    output: {
        path: __dirname + '/public',
        publicPath:  './',
        filename: '[name].js'
    },
    plugins: [
        new HtmlWebpackPlugin({
            alwaysWriteToDisk: true,
            title: 'Igloo',
            filename: 'index.html',
            template: './src/index.html'
        }),
        new FaviconsWebpackPlugin({
            logo: './src/images/app_icon.png',
            icons: {
                android: false,
                appleIcon: false,
                appleStartup: false,
                coast: false,
                favicons: true,
                firefox: false,
                opengraph: false,
                twitter: false,
                yandex: false,
                windows: false
            }})
    ]
};